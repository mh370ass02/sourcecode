#pragma once
#ifndef _LIB
#define _LIB

struct Date 
{
	char day;
	char month;
	short int year;
};

struct DateTime
{
	char minute;
	char hour;
	char day;
	char month;
	short int year;
};

struct BookBorrowForUser 
{
	int bookId;
	DateTime borrowDay;
	DateTime returnDay;
};
struct BookBorrowForBook
{
	int userId;
	DateTime borrowDay;
	DateTime returnDay;
};

struct User
{
	long long int userId;
	unsigned char userName[64];
	unsigned char userCMND[12];
	Date userBirthday;
	char userDepartment[50];
	char userAddress[100];
	char userMoreInfo[100];

	char account[20];
	char password[65];
	bool active = true;
	char role = 1;
	BookBorrowForUser bookBorrow[20];
	int bookBorrowNum = 0;
	char notification[2000];
	/*
	Binary mode:
	role = 0001 (1): Doc gia;
	role = 0010 (2): Thu thu;
	role = 0100 (4): Quan ly nguoi dung;
	role = 1000 (8) : Admin (full control);
	Cac tai khoan co nhieu vai tro bang cach cong cac ma role lai;
	*/
};

struct Book
{
	int bookID;
	char bookName[100];
	char bookType[50];
	char bookSummary[200];
	char bookAuthor[200];
	char bookPublish[200];
	BookBorrowForBook bookBorrow[1000];
	int bookBorrowUserNum = 0;
	int bookTotalNum;
	char bookInfoMore[200];
	char bookStar = -1;
	int bookVoteNum = 0;
};



#endif